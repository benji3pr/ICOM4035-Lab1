package orderedStructures;

public class Arithmetic extends Progression {
	private double commonDifference; 
	
	public Arithmetic(double firstValue, double commonDifference) { 
		super(firstValue); 
		this.commonDifference = commonDifference; 
	}
	
	@Override
	public double nextValue() throws IllegalStateException {
		if(state == false) throw new IllegalStateException("firstValue method has not been executed");
		current = current + commonDifference; 
		return current;
	}
	
	public String toString(){
		return "Arith("+(int)this.firstValue()+", "+(int)this.commonDifference+")";
		
	}
	
	public double getTerm(int n){
		double result = this.firstValue()+(n*this.commonDifference)-this.commonDifference;
		return result;
		
	}

}
