package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor; 
	
	public Geometric(double firstValue, double commonFactor) { 
		super(firstValue); 
		this.commonFactor = commonFactor; 
	}
	
	@Override
	public double nextValue() throws IllegalStateException {
		if(state == false) throw new IllegalStateException("firstValue method has not been executed");
		current = current * commonFactor; 
		return current;
	}
	
	public String toString(){
		return "Geom("+(int)this.firstValue()+", "+(int)this.commonFactor+")";
		
	}
	
	public double getTerm(int n){
		double result = this.firstValue()*(Math.pow(this.commonFactor, n-1));
		return result;
		
	}

}
